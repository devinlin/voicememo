<!--
- SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
- SPDX-License-Identifier: GPL-3.0-or-later
-->

# krecorder
A cross-platform audio recorder built with Kirigami + Qt Quick technologies.
